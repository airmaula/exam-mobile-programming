const provinces = [
    {
        "name": "Aceh",
        "latitude": 4.6951,
        "longitude": 96.7494
    },
    {
        "name": "Sumatera Utara",
        "latitude": 2.1150,
        "longitude": 99.5451
    },
    {
        "name": "Sumatera Barat",
        "latitude": -0.7893,
        "longitude": 100.6787
    },
    {
        "name": "Riau",
        "latitude": 0.5090,
        "longitude": 101.4475
    },
    {
        "name": "Jambi",
        "latitude": -1.4852,
        "longitude": 102.4381
    },
    {
        "name": "Sumatera Selatan",
        "latitude": -3.3194,
        "longitude": 103.9140
    },
    {
        "name": "Bengkulu",
        "latitude": -3.7928,
        "longitude": 102.2608
    },
    {
        "name": "Lampung",
        "latitude": -4.5586,
        "longitude": 105.4068
    },
    {
        "name": "Kepulauan Bangka Belitung",
        "latitude": -2.7410,
        "longitude": 106.4406
    },
    {
        "name": "Kepulauan Riau",
        "latitude": 3.9456,
        "longitude": 108.1429
    },
    {
        "name": "DKI Jakarta",
        "latitude": -6.2088,
        "longitude": 106.8456
    },
    {
        "name": "Jawa Barat",
        "latitude": -6.9147,
        "longitude": 107.6098
    },
    {
        "name": "Jawa Tengah",
        "latitude": -7.1509,
        "longitude": 110.1403
    },
    {
        "name": "Daerah Istimewa Yogyakarta",
        "latitude": -7.8012,
        "longitude": 110.3649
    },
    {
        "name": "Jawa Timur",
        "latitude": -7.5361,
        "longitude": 112.7126
    },
    {
        "name": "Banten",
        "latitude": -6.4058,
        "longitude": 106.0640
    },
    {
        "name": "Bali",
        "latitude": -8.3405,
        "longitude": 115.0920
    },
    {
        "name": "Nusa Tenggara Barat",
        "latitude": -8.6526,
        "longitude": 117.3611
    },
    {
        "name": "Nusa Tenggara Timur",
        "latitude": -8.6189,
        "longitude": 121.0009
    },
    {
        "name": "Kalimantan Barat",
        "latitude": 0.2788,
        "longitude": 111.4753
    },
    {
        "name": "Kalimantan Tengah",
        "latitude": -1.6815,
        "longitude": 113.3824
    },
    {
        "name": "Kalimantan Selatan",
        "latitude": -3.3194,
        "longitude": 103.9140
    }
]

export default provinces