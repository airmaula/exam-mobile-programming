import {Dimensions, Image, Linking, ScrollView, StyleSheet, View} from 'react-native';
import {PaperProvider, DefaultTheme, Card, Text, Appbar, Button, TextInput} from 'react-native-paper'
import {useEffect, useMemo, useRef, useState} from "react";
import axios from "axios";
import MapView, {Marker} from "react-native-maps";
import _ from 'lodash'
import provinces from "./data/provinces";
import cities from "./data/cities";

const theme = {
    ...DefaultTheme,
    colors: {
        ...DefaultTheme.colors,
        primary: 'tomato',
        secondary: 'yellow',
    },
};

const grouppedProvinces = _.uniq(provinces.map(item => item.name))

export default function App() {

    const [state, setState] = useState({
        hospitals: [],
        hospitalsPerProvince: [],
        activeMarker: null,
        isLoaded: false,
        keyword: '',
    })

    const mapRef = useRef(null)

    const getData = async () => {
        try {
            const response = await axios.get("https://dekontaminasi.com/api/id/covid19/hospitals")

            setState(prevState => ({
                ...prevState,
                hospitals: response.data,
                hospitalsPerProvince: grouppedProvinces.map(prov => ({
                    province: prov,
                    hospitals: response.data.filter(item => item.province === prov)
                })).sort((a, b) => a.province - b.province),
                isLoaded: true,
            }))
        } catch (e) {
            alert(e.request.response.message)
        }
    }

    const setMapToProvince = (province) => {
        const item = provinces.find(prov => prov.name === province)
        if (item) {
            mapRef.current.animateToRegion({
                latitude: item.latitude,
                longitude: item.longitude,
                latitudeDelta: 10,
                longitudeDelta: 10,
            }, 1000)

            setState(prevState => ({
                ...prevState,
                activeMarker: item,
            }))
        } else {
            alert('Provinsi belum tersedia')
        }
    }

    const setMapToRegion = (city) => {
        const item = cities.find(c => c.name === city.split(',')[0])
        if (item) {
            mapRef.current.animateToRegion({
                latitude: item.latitude,
                longitude: item.longitude,
                latitudeDelta: 10,
                longitudeDelta: 10,
            }, 1000)

            setState(prevState => ({
                ...prevState,
                activeMarker: item,
            }))
        } else {
            alert('Kota belum tersedia')
        }
    }

    const hospitalsPerProvince = useMemo(() => {
        return state.hospitalsPerProvince.map(item => {
            item.filteredHospitals = item.hospitals.filter(hospital => hospital.name.toLowerCase().search(state.keyword.toLowerCase()) > -1 || hospital.region.toLowerCase().search(state.keyword.toLowerCase()) > -1)
            return item
        })
    }, [state.hospitalsPerProvince, state.keyword])

    useEffect(() => {
        (async () => {
            await getData()
        })()
    }, []);

    return (
        <PaperProvider theme={theme}>
            <Appbar.Header>
                <Appbar.Content title="Covid19 CareHub" />
            </Appbar.Header>

            <MapView style={styles.map} initialRegion={{
                latitude: -0.7893,
                longitude: 113.9213,
                latitudeDelta: 50,
                longitudeDelta: 50,
            }} ref={mapRef}
            >
                {
                    state.activeMarker && (
                        <Marker
                            coordinate={{ latitude: state.activeMarker?.latitude, longitude: state.activeMarker?.longitude }}
                            title={state.activeMarker?.name}
                            description={`Province in Indonesia: ${state.activeMarker?.name}`}
                        />
                    )
                }
            </MapView>
            {
                state.isLoaded ? (
                    <View style={styles.container}>
                        <TextInput
                            label="Cari Nama Rumah Sakit atau Daerah"
                            value={state.keyword}
                            onChangeText={text => setState(prevState => ({...prevState, keyword: text,}))}
                            outlineColor={'#e0004d'}
                            activeOutlineColor={'#e0004d'}
                            textColor={'#e0004d'}
                            activeUnderlineColor={'#e0004d'}
                            mode={'outlined'}
                            style={styles.search}
                        />

                        <ScrollView style={{
                            height: Dimensions.get('window').height - 300 - 16 - 16 - 200
                        }}>
                            {
                                (state.keyword !== '' ? hospitalsPerProvince.filter(item => item.filteredHospitals.length > 0) : hospitalsPerProvince).map((item, key) => (
                                    <View key={key}>
                                        <Text variant={'bodyLarge'} style={styles.provinceName} onTouchEnd={() => setMapToProvince(item.province)}>{item.province}</Text>
                                        {
                                            (state.keyword !== '' ? item.filteredHospitals : item.hospitals)?.map((hospital, i) => (
                                                <Card key={i} style={styles.card} mode={'outlined'}>
                                                    <Card.Content>
                                                        <Image source={{
                                                            uri: `https://exam-mp.irfanstudio.com/rs-images/?image=${hospital.name}.png`,
                                                        }} style={{width: '100%', height: 200, marginBottom: 16,}}/>
                                                        <Text variant="bodySmall">{hospital.region}</Text>
                                                        <Text variant="titleMedium">{hospital.name}</Text>
                                                        <Text variant="bodySmall">{hospital.address}, {hospital.phone}</Text>

                                                        <View>
                                                            <Button icon="google-maps" buttonColor={'#e0004d'} mode="contained" style={{...styles.cta, marginBottom: 12, marginTop: 16}} onPress={() => setMapToRegion(hospital.region)}>
                                                                Lihat Lokasi
                                                            </Button>
                                                            <Button icon="phone" buttonColor={'transparent'} textColor={'#e0004d'} mode="outlined" style={styles.cta} onPress={() => {
                                                                try {
                                                                    Linking.openURL(`tel://${hospital.phone}`)
                                                                } catch (error) {
                                                                    console.log('Error opening phone number:', error)
                                                                }
                                                            }}>
                                                                Hubungi Rumah Sakit
                                                            </Button>
                                                        </View>
                                                    </Card.Content>
                                                </Card>
                                            ))
                                        }
                                    </View>
                                ))
                            }
                        </ScrollView>
                    </View>
                ) : (
                    <View>
                        <Text style={styles.loader} variant={'bodyMedium'}>
                            Memuat data...
                        </Text>
                    </View>
                )
            }
        </PaperProvider>
    );
}

const styles = StyleSheet.create({
    container: {
        padding: 16
    },
    card: {
        marginBottom: 16
    },
    map: {
        width: Dimensions.get('window').width,
        height: 300,
    },
    provinceName: {
        marginBottom: 12,
        fontWeight: 'bold'
    },
    loader: {
        textAlign: 'center',
        padding: 24,
    },
    cta: {
        borderRadius: 6,
    },
    search: {
        marginBottom: 16
    }
});
